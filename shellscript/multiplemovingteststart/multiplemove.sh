#!/bin/bash

# Gziping file and multiple moving file with extension gz.

gzip -r testzip
#gzip -dr testzip

mv /home/Leonhard/projects/bashing/shellscript/multiplemovingteststart/testzip/*.gz /home/Leonhard/projects/bashing/shellscript/multipledestination
cd /home/Leonhard/projects/bashing/shellscript/multipledestination

echo Hello from shellscript.