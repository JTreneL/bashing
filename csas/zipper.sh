#!/usr/bin/bash

# Script do crona, který se spustí k prvnímu datu v měsíci.
# Provede komprimaci souborů za minulý měsíc.

# CRONTAB: První den v měsíci 4:00 ráno.
# 0 4 1 * * /home/Leonhard/Newprojects/Bash/csas/zipper.sh

# Počet dnů v měsíci minulý měsíc.
DNY=$(cal $(date -d "$(date +%Y-%m-1) -1 month" +%Y-%m-1) | awk 'NF {DAYS = $NF}; END {print DAYS}')

# Aktualní čas ve formátu ROK,MĚSÍC,DEN.
NOW=$(date +"%Y-%m-%d")

# Aktualní čas ve formátu HOD,MIN.
NOW_H_M=$(date +"%H-%M")

# Cesta ke složce, kde se má gzipovat, script bere jen soubory s koncovkou .log .
FILES="/home/Leonhard/Newprojects/Bash/csas/A/*.log"

# Cesta ke složce s logy z výstupem ze scriptu.
LOG="/home/Leonhard/Newprojects/Bash/csas/LOG/log-$NOW.log"


echo "Čas: $NOW_H_M" >> $LOG 2>&1


for f in $FILES; do
  if [ -f "$f" ]; then
  find $f -type f -mtime +$DNY -exec gzip {} \; -exec echo "Komprimuji soubor:" $(basename ${f}) >> $LOG 2>&1 \;
  else
  echo "Files with extensions .log is not found." >> $LOG 2>&1
  fi
done


echo "-------------------------------------[KONEC]-------------------------------------" >> $LOG 2>&1

